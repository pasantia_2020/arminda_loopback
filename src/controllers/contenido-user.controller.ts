import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {ContenidoUser} from '../models';
import {ContenidoUserRepository} from '../repositories';

export class ContenidoUserController {
  constructor(
    @repository(ContenidoUserRepository)
    public contenidoUserRepository: ContenidoUserRepository,
  ) {}

  @post('/contenido-users', {
    responses: {
      '200': {
        description: 'ContenidoUser model instance',
        content: {'application/json': {schema: getModelSchemaRef(ContenidoUser)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContenidoUser, {
            title: 'NewContenidoUser',
            exclude: ['id_contenido'],
          }),
        },
      },
    })
    contenidoUser: Omit<ContenidoUser, 'iid_contenidod'>,
  ): Promise<ContenidoUser> {
    return this.contenidoUserRepository.create(contenidoUser);
  }

  @get('/contenido-users/count', {
    responses: {
      '200': {
        description: 'ContenidoUser model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ContenidoUser) where?: Where<ContenidoUser>,
  ): Promise<Count> {
    return this.contenidoUserRepository.count(where);
  }

  @get('/contenido-users', {
    responses: {
      '200': {
        description: 'Array of ContenidoUser model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ContenidoUser, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ContenidoUser) filter?: Filter<ContenidoUser>,
  ): Promise<ContenidoUser[]> {
    return this.contenidoUserRepository.find(filter);
  }

  @patch('/contenido-users', {
    responses: {
      '200': {
        description: 'ContenidoUser PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContenidoUser, {partial: true}),
        },
      },
    })
    contenidoUser: ContenidoUser,
    @param.where(ContenidoUser) where?: Where<ContenidoUser>,
  ): Promise<Count> {
    return this.contenidoUserRepository.updateAll(contenidoUser, where);
  }

  @get('/contenido-users/{id}', {
    responses: {
      '200': {
        description: 'ContenidoUser model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ContenidoUser, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ContenidoUser, {exclude: 'where'}) filter?: FilterExcludingWhere<ContenidoUser>
  ): Promise<ContenidoUser> {
    return this.contenidoUserRepository.findById(id, filter);
  }

  @patch('/contenido-users/{id}', {
    responses: {
      '204': {
        description: 'ContenidoUser PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContenidoUser, {partial: true}),
        },
      },
    })
    contenidoUser: ContenidoUser,
  ): Promise<void> {
    await this.contenidoUserRepository.updateById(id, contenidoUser);
  }

  @put('/contenido-users/{id}', {
    responses: {
      '204': {
        description: 'ContenidoUser PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() contenidoUser: ContenidoUser,
  ): Promise<void> {
    await this.contenidoUserRepository.replaceById(id, contenidoUser);
  }

  @del('/contenido-users/{id}', {
    responses: {
      '204': {
        description: 'ContenidoUser DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.contenidoUserRepository.deleteById(id);
  }
}
