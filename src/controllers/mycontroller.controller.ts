// Uncomment these imports to begin using these cool features!

import {Binding, Context, ValueFactory} from '@loopback/core';

const context = new Context();
const binding = Binding.bind('my-key');
binding.to('my-value');

binding.toDynamicValue(() => 'my-value');
binding.toDynamicValue(() => new Date());
binding.toDynamicValue(() => Promise.resolve('my-value'));



// The factory function now have access extra metadata about the resolution
const factory: ValueFactory<string> = resolutionCtx => {
  return `Hello, ${resolutionCtx.context.name}#${resolutionCtx.binding.key
    } ${resolutionCtx.options.session?.getBindingPath()}`;
};
const b = context.bind('msg').toDynamicValue(factory);

export class MycontrollerController {


}
