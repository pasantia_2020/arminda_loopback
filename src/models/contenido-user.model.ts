import {Entity, model, property} from '@loopback/repository';

@model()
export class ContenidoUser extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id_contenido?: number;

  @property({
    type: 'number',
    required: true,
  })
  id_usuario: number;

  @property({
    type: 'number',
    required: true,
  })
  id_publicacion: number;


  constructor(data?: Partial<ContenidoUser>) {
    super(data);
  }
}

export interface ContenidoUserRelations {
  // describe navigational properties here
}

export type ContenidoUserWithRelations = ContenidoUser & ContenidoUserRelations;
