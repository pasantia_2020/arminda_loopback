import {Entity, model, property} from '@loopback/repository';

@model()
export class Plataforma extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id_plataforma?: number;

  @property({
    type: 'string',
    required: true,
  })
  nombre: string;


  constructor(data?: Partial<Plataforma>) {
    super(data);
  }
}

export interface PlataformaRelations {
  // describe navigational properties here
}

export type PlataformaWithRelations = Plataforma & PlataformaRelations;
