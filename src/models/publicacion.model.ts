import {Entity, model, property} from '@loopback/repository';

@model()
export class Publicacion extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id_publicacion?: number;

  @property({
    type: 'string',
    required: true,
  })
  id_plataforma: string;

  @property({
    type: 'string',
    required: true,
  })
  nombre: string;

  @property({
    type: 'date',
    required: true,
  })
  tiempo: string;

  @property({
    type: 'string',
    required: true,
  })
  descripcion: string;


  constructor(data?: Partial<Publicacion>) {
    super(data);
  }
}

export interface PublicacionRelations {
  // describe navigational properties here
}

export type PublicacionWithRelations = Publicacion & PublicacionRelations;
