import {DefaultCrudRepository} from '@loopback/repository';
import {ContenidoUser, ContenidoUserRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ContenidoUserRepository extends DefaultCrudRepository<
  ContenidoUser,
  typeof ContenidoUser.prototype.id_contenido,
  ContenidoUserRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(ContenidoUser, dataSource);
  }
}
