import {DefaultCrudRepository} from '@loopback/repository';
import {Plataforma, PlataformaRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PlataformaRepository extends DefaultCrudRepository<
  Plataforma,
  typeof Plataforma.prototype.id_plataforma,
  PlataformaRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Plataforma, dataSource);
  }
}
